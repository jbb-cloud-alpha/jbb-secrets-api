package org.jbb.cloud.secrets.api.v1;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PasswordDto {

  private String memberId;

  private String passwordHash;

}
