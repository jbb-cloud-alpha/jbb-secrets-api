package org.jbb.cloud.secrets.api.v1.exception;

public class PasswordNotFoundException extends SecretsException {

  public PasswordNotFoundException(String memberId) {
    super(String.format("Password for member with id '%s' is not found", memberId));
  }

}
