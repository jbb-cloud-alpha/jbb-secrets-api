package org.jbb.cloud.secrets.api.v1;

import lombok.RequiredArgsConstructor;
import org.jbb.cloud.secrets.api.v1.exception.PasswordNotFoundException;
import org.jbb.cloud.secrets.api.v1.exception.PasswordPolicyException;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PasswordResource {

  private final PasswordsClient passwordsClient;

  @Retryable
  public PasswordDto getPassword(String memberId) throws PasswordNotFoundException {
    return passwordsClient.getPassword(memberId);
  }

  @Retryable
  public PasswordDto savePassword(SavePasswordDto passwordDto) throws PasswordPolicyException {
    return passwordsClient.savePassword(passwordDto);
  }

  @Retryable
  public PasswordDto editPassword(SavePasswordDto passwordDto) throws PasswordPolicyException {
    return passwordsClient.editPassword(passwordDto);
  }

  @Retryable
  public void deletePassword(String memberId) throws PasswordNotFoundException {
    passwordsClient.deletePassword(memberId);
  }
}
