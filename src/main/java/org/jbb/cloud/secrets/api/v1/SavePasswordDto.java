package org.jbb.cloud.secrets.api.v1;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SavePasswordDto {

  @NotBlank
  private String memberId;

  @NotBlank
  private String password;

}
