package org.jbb.cloud.secrets.api.v1;

import javax.validation.Valid;
import org.jbb.cloud.secrets.api.v1.exception.PasswordNotFoundException;
import org.jbb.cloud.secrets.api.v1.exception.PasswordPolicyException;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "jbb-secrets", url = "${jbb.secrets.url:}")
interface PasswordsClient {

  String V1_PASSWORDS = "/v1/passwords";

  @RequestMapping(path = V1_PASSWORDS, method = RequestMethod.GET)
  PasswordDto getPassword(@RequestParam("memberId") String memberId)
      throws PasswordNotFoundException;

  @RequestMapping(path = V1_PASSWORDS, method = RequestMethod.POST)
  PasswordDto savePassword(@RequestBody @Valid SavePasswordDto passwordDto)
      throws PasswordPolicyException;

  @RequestMapping(path = V1_PASSWORDS, method = RequestMethod.PUT)
  PasswordDto editPassword(@RequestBody @Valid SavePasswordDto passwordDto)
      throws PasswordPolicyException;

  @RequestMapping(path = V1_PASSWORDS, method = RequestMethod.DELETE)
  void deletePassword(@RequestParam("memberId") String memberId) throws PasswordNotFoundException;

}
