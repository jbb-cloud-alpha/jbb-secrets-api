package org.jbb.cloud.secrets.api.v1.exception;

public class SecretsException extends Exception {

  public SecretsException(String message) {
    super(message);
  }
}
