package org.jbb.cloud.secrets.api.v1.exception;

public class PasswordPolicyException extends SecretsException {

  public PasswordPolicyException(String message) {
    super(message);
  }
}
